**Make sure you have docker and docker-compose installed.**

* copy the .env.dist to .env with : 

`cp .env.dist .env`

* then run the docker-composer in the project folder with : 

`docker-compose up`


* after cloning, run the following command to install the dependencies needed for laravel.

`docker run --rm -v $(pwd):/app composer install`
or
`docker-compose exec php composer install`


(check this page if your docker doesn't start : https://docs.docker.com/engine/install/linux-postinstall/ )

then you can access the page by http://localhost:888

* if you get a permission error you can try teh following in the project folder
 
`sudo chmod -R 777 storage`

* After that you should run the migrations for the tables to be created: 

`docker-compose exec php /var/www/app/artisan migrate`

